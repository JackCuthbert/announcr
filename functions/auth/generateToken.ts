import { Request, Response } from 'express'
import Boom from 'boom'
import * as jwt from 'jsonwebtoken'
import { jwtSecret } from '../../config'
import { User } from '../../models'

export async function generateToken (req: Request, res: Response) {
  const userRepo = req.db.getRepository(User)
  try {
    const user = await userRepo.findOne(req.user)
    if (user === undefined) {
      const { output } = Boom.forbidden('User not found')
      return res.status(output.statusCode).send(output)
    }

    const jwtOpts: jwt.SignOptions = {
      // TODO: Other registered claims?
      subject: String(user.id),
      expiresIn: '1 day'
    }

    // TODO: Enfore a specific type
    const token = jwt.sign({
      displayName: user.displayName,
      email: user.email
    }, jwtSecret, jwtOpts)

    res.send(token)
  } catch (error) {
    console.log(error)
    const { output } = Boom.boomify(error)
    res.status(output.statusCode).send(output)
  }
}
