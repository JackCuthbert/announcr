import modofun, { arity } from 'modofun'
import cors from 'cors'
import { method, dbConn, passport } from '../../middleware'
import { generateToken } from './generateToken'

/**
 * Authentication controller
 */
export const auth = modofun({
  // POST /auth/login
  login: [
    method('post'),
    arity(0),
    passport.authenticate('local-login', { session: false }),
    generateToken
  ],
  // POST /auth/signup
  signup: [
    method('post'),
    arity(0),
    passport.authenticate('local-signup', { session: false }),
    generateToken
  ]
}, {
  type: 'gcloud',
  mode: 'reqres',
  middleware: [cors(), passport.initialize(), dbConn],
  // NOTE: Need to implement a custom error handler as the default will always
  // be called wiuth a 500 when passport fails to authenticate a user.
  errorHandler: (error, _, res) => {
    res.status(401).send({
      statusCode: 401,
      error: 'Unauthorized',
      message: error
    })
  }
})
