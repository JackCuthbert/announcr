import { Request, Response } from 'express'
import * as Yup from 'yup'
import Boom from 'boom'
import { UserRepository } from '../../repositories'

/**
 * Search the users on our database by exact email, returning their email
 *
 * I imagine this will be used to invite users to the platform or team
 */
export async function searchUsers (req: Request, res: Response) {
  try {
    const bodySchema = Yup.object({
      q: Yup.string().required()
    })
    await bodySchema.validate(req.query)
  } catch (error) {
    const { output, data } = Boom.badRequest(error.name, error.errors)
    return res.status(output.statusCode).send({ ...output.payload, data })
  }

  try {
    const userRepository = req.db.getCustomRepository(UserRepository)
    const users = await userRepository.getUserByEmail(req.query.q, [
      'id',
      'email',
      'displayName'
    ])

    res.status(200).send(users).end()
  } catch (error) {
    console.error(error)
    const { output } = Boom.boomify(error)
    res.status(output.statusCode).send(output.payload).end()
  }
}
