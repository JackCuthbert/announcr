import modofun, { arity } from 'modofun'
import cors from 'cors'

import { checkJwt, method, dbConn } from '../../middleware'
import { searchUsers } from './searchUsers'
import { viewProfile } from './viewProfile'
import { editProfile } from './editProfile'

/**
 * User resource controller
 */
export const user = modofun({
  // GET /user/profile
  profile: [method('get'), arity(0), viewProfile],
  // POST /user/edit
  edit: [method('post'), arity(0), editProfile],
  // GET /user/search
  search: [method('get'), arity(0), searchUsers]
}, {
  type: 'gcloud',
  mode: 'reqres',
  middleware: [cors(), checkJwt, dbConn]
})
