import { Request, Response } from 'express'
import * as Yup from 'yup'
import Boom from 'boom'
import { User } from '../../models'

/**
 * Edit the user's profile on Auth0 (email, password, and metadata only)
 */
export async function editProfile (req: Request, res: Response) {
  try {
    const bodySchema = Yup.object({
      email: Yup.string().email('email must be a valid email address')
    })
    await bodySchema.validate(req.body)
  } catch (error) {
    const boom = Boom.badRequest(error.name, error.errors)
    return res.status(boom.output.statusCode).send({
      ...boom.output.payload,
      data: boom.data
    })
  }

  let user
  const userRepository = req.db.getRepository(User)
  try {
    user = await userRepository.findOneOrFail(req.user, {
      select: ['id','email']
    })
  } catch (error) {
    console.error(error)
    const { output } = Boom.boomify(error)
    return res.status(output.statusCode).send(output.payload)
  }

  res.status(200).send(user).end()
}
