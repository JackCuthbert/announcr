import { Request, Response } from 'express'
import Boom from 'boom'
import { User } from '../../models'

/**
 * Return the user's profile from Auth0
 */
export async function viewProfile (req: Request, res: Response) {
  try {
    const userRepository = req.db.getRepository(User)
    const user = await userRepository.findOne(req.user, {
      select: ['id', 'email', 'displayName']
    })

    if (user === undefined) {
      const { output } = Boom.notFound('User profile not found')
      return res.status(output.statusCode).send(output.payload)
    }

    res.status(200).send(user)
  } catch (error) {
    console.error(error)
    const { output } = Boom.boomify(error)
    return res.status(output.statusCode).send(output.payload)
  }
}
