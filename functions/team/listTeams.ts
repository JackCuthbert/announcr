import { Request, Response } from 'express'
import Boom from 'boom'
import { TeamRepository } from '../../repositories'

/**
 * List all the teams with their respective users that a user either owns or
 * belongs to
 */
export async function listTeams (req: Request, res: Response) {
  try {
    const teamRepository = req.db.getCustomRepository(TeamRepository)
    const allTeams = await teamRepository.getTeamsForUser(req.user)

    res.send(allTeams)
  } catch (error) {
    console.log(error)
    const boom = Boom.boomify(error)
    return res.status(boom.output.statusCode).send(boom.output.payload)
  }
}
