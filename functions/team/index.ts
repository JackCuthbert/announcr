import modofun, { arity } from 'modofun'
import cors from 'cors'
import { checkJwt, method, dbConn } from '../../middleware'

import { createTeam } from './createTeam'
import { listTeams } from './listTeams'
import { deleteTeam } from './deleteTeam'
import { viewTeam } from './viewTeam'
import { addUser } from './addUser'

/**
 * Team resource controller
 */
export const team = modofun({
  // /team/create
  create: [method('post'), arity(0), createTeam],
  // GET /team/list
  list: [method('get'), arity(0), listTeams],
  // GET /team/view/{id}
  view: [method('get'), arity(1), viewTeam],
  // DELETE /team/delete/{id}
  delete: [method('delete'), arity(1), deleteTeam],
  // POST /team/add_user
  add_user: [method('post'), arity(0), addUser]
}, {
  type: 'gcloud',
  mode: 'reqres',
  middleware: [cors(), checkJwt, dbConn]
})
