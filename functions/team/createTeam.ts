import { Request, Response } from 'express'
import * as Yup from 'yup'
import Boom from 'boom'
import { Team, User, Category } from '../../models'

/** Create a new team and make the requester the owner */
export async function createTeam (req: Request, res: Response) {
  try {
    const bodySchema = Yup.object({
      name: Yup.string().min(1, 'Team names must contain at least one character').required()
    })
    await bodySchema.validate(req.body)
  } catch (error) {
    const boom = Boom.badRequest(error.name, error.errors)
    return res.status(boom.output.statusCode).send({
      ...boom.output.payload,
      data: boom.data
    })
  }

  const userRepository = req.db.getRepository(User)
  const teamRepository = req.db.getRepository(Team)
  const categoryRepository = req.db.getRepository(Category)

  try {
    const user = await userRepository.findOneOrFail(req.user)

    const team = new Team()
    team.name = req.body.name
    team.owner = user
    team.users = [user]

    // NOTE: Every new team needs a default category, we have to save the team
    // first so the association works
    const newTeam = await teamRepository.save(team)

    const category = new Category()
    category.name = 'Default Category'
    category.isDefault = true
    category.team = newTeam

    // Update the remaining relationships
    // TODO: Can this be achieved with cascades?
    await Promise.all([
      categoryRepository.save(category),
      userRepository.save(user)
    ])

    res.send({ id: newTeam.id, name: newTeam.name })
  } catch (error) {
    console.error(error)
    res.status(500).send('Unable to save Team in database')
  }
}
