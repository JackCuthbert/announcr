import { Request, Response } from 'express'
import Boom from 'boom'
import { TeamRepository } from '../../repositories'

/**
 * Return a team that a user owns or belongs to
 */
export async function viewTeam (req: Request, res: Response) {
  const [teamId] = req.params
  try {
    const teamRepository = req.db.getCustomRepository(TeamRepository)
    const team = await teamRepository.getTeamForUser(Number(teamId), req.user)

    if (team === undefined) {
      const { output } = Boom.notFound('Team not found')
      return res.status(output.statusCode).send(output.payload)
    }
    res.send(team)
  } catch (error) {
    console.log(error)
    const { output } = Boom.boomify(error)
    res.status(output.statusCode).send(output.payload)
  }
}
