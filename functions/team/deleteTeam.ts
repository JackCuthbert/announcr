import { Request, Response } from 'express'
import Boom from 'boom'
import * as Yup from 'yup'
import { Team } from '../../models'

/**
 * Delete a team
 *
 * DELETE /team/delete/{id}
 */
export async function deleteTeam (req: Request, res: Response) {
  const [teamId] = req.params

  try {
    const schema = Yup.number().required('Team ID is required')
    await schema.validate(teamId)
  } catch (error) {
    const boom = Boom.badRequest(error.name, error.errors)
    return res.status(boom.output.statusCode).send({
      ...boom.output.payload,
      data: boom.data
    })
  }

  const teamRepository = req.db.getRepository(Team)
  const team = await teamRepository.findOne(teamId, { relations: ['owner'] })
  if (team === undefined) {
    const boom = Boom.notFound('Team not found')
    return res.status(boom.output.statusCode).send(boom.output.payload)
  }

  if (team.owner.id !== req.user) {
    const boom = Boom.unauthorized('Teams can only be deleted by the owner')
    return res.status(boom.output.statusCode).send(boom.output.payload)
  }

  try {
    const { owner, ...deleted } = await teamRepository.remove(team)
    return res.status(200).send(deleted)
  } catch (error) {
    console.log(error)
    const boom = Boom.boomify(error)
    return res.status(boom.output.statusCode).send(boom.output.payload)
  }
}
