import { Request, Response } from 'express'
import * as Yup from 'yup'
import Boom from 'boom'
import { Team } from '../../models'
import { UserRepository } from '../../repositories'

/**
 * POST
 *
 * /team/add_user
 */
export async function addUser (req: Request, res: Response) {
  try {
    const bodySchema = Yup.object({
      teamId: Yup.number().required(),
      email: Yup.string().email().required()
    })
    await bodySchema.validate(req.body)
  } catch (error) {
    const { output, data } = Boom.badRequest(error.name, error.errors)
    return res.status(output.statusCode).send({ ...output.payload, data })
  }

  const teamRepository = req.db.getRepository(Team)
  const userRepository = req.db.getCustomRepository(UserRepository)

  try {
    const team = await teamRepository.findOne(req.body.teamId, {
      relations: ['owner', 'users']
    })
    if (team === undefined) {
      const { output } = Boom.notFound('Team not found')
      return res.status(output.statusCode).send(output.payload)
    }

    if (team.owner.id !== req.user) {
      const { output } = Boom.unauthorized('Unable to invite user to team you do not own')
      return res.status(output.statusCode).send(output.payload)
    }

    const user = await userRepository.getUserByEmail(req.body.email)
    if (user === undefined) {
      const { output } = Boom.notFound('User not found')
      return res.status(output.statusCode).send(output.payload)
    }

    if (team.users.find(u => u.email === user.email)) {
      const { output } = Boom.conflict('User already exists in team')
      return res.status(output.statusCode).send(output.payload)
    }

    team.users = [...team.users, user]
    const saved = await teamRepository.save(team)

    // NOTE: Remove teams and hashed password from payload
    const payload = saved.users.map(({ teams, password, ...pl }) => pl)
    res.status(200).send(payload).end()
  } catch (error) {
    console.error(error)
    const { output } = Boom.boomify(error)
    res.status(output.statusCode).send(output.payload).end()
  }
}
