import { Request, Response, NextFunction } from 'express'
import Boom from 'boom'

type Method = 'get' | 'post' | 'put' | 'delete' | 'patch' | 'options'

/**
 * Assert that the route is using a particular method
 */
export function method (method: Method) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (req.method.toLowerCase() !== method.toLowerCase()) {
      const { output } = Boom.methodNotAllowed()
      return res.status(output.statusCode).send(output.payload).end()
    }
    next()
  }
}
