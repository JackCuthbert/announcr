/**
 * NOTE: This file re-exports passport after setting up any strategies that the
 * passport middleware will need. When using Passport middleware don't import
 * from the base module and always import from here.
 *
 * ie. `import { passport } from '../middleware'`
 *
 * NOTE: `req.user` is _always_ populated with the primary column identifier of
 * the user. See module augmentation.
 */

declare module 'express' {
  export interface Request {
    user: string
  }
}

import bcrypt from 'bcrypt'
import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { User } from '../models'

/** bcrypt options */
const crypt = {
  saltRounts: 10
}

const loginStrategy = new LocalStrategy({
  session: false,
  passReqToCallback: true
}, (req, username, password, done) => {
  const repo = req.db.getRepository(User)
  repo.findOne({ where: { email: username } })
    .then(async user => {
      if (user === undefined) return done('User not found')

      const match = await bcrypt.compare(password, user.password)
      if (!match) return done('Incorrect password')
      return done(null, user.id)
    })
    .catch(error => done(error))
})

const signupStrategy = new LocalStrategy({
  session: false,
  passReqToCallback: true
}, (req, username, password, done) => {
  const repo = req.db.getRepository(User)
  repo.findOne({ where: { email: username } })
    .then(async user => {
      if (user !== undefined) return done('Email address in use')

      const incomingHash = await bcrypt.hash(password, crypt.saltRounts)
      const newUser = new User()
      newUser.email = username
      newUser.password = incomingHash
      const saved = await repo.save(newUser)
      return done(null, saved.id)
    })
    .catch(error => done(error))
})

passport.use('local-login', loginStrategy)
passport.use('local-signup', signupStrategy)

export { passport }
