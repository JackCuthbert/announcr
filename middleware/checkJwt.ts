import { Request, Response, NextFunction } from 'express'
import Boom from 'boom'
import { verify } from 'jsonwebtoken'
import { jwtSecret } from '../config'

interface JWTPayload {
  displayName: string | null
  email: string
  iat: number
  exp: number
  sub: string
}

/**
 * Assert a generic type for the returned value of `jwt.verify()`
 * @param token
 * @param secret
 */
function verifyToken<T = {}> (token: string, secret: string): T {
  return verify(token, secret) as unknown as T
}

/**
 * Verify that a request contains a valid JWT
 */
export function checkJwt (req: Request, res: Response, next: NextFunction) {
  if (req.headers.authorization === undefined) {
    const { output } = Boom.forbidden('Missing authorization header')
    return res.status(output.statusCode).send(output.payload).end()
  }

  const [type, token] = req.headers.authorization.split(' ')

  if (type !== 'Bearer') {
    const { output } = Boom.badRequest('Authorization header must be of type "Bearer"')
    return res.status(output.statusCode).send(output.payload)
  }

  try {
    const verified = verifyToken<JWTPayload>(token, jwtSecret)
    req.user = verified.sub
    next()
  } catch (error) {
    console.error(error)
    const { output } = Boom.unauthorized('Invalid token')
    return res.status(output.statusCode).send(output.payload)
  }
}
