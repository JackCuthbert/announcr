import { Request, Response, NextFunction } from 'express'
import { Connection, createConnection } from 'typeorm'
import { connectionOptions } from '../config'

// Augment the request interface to include a db property
declare module 'express' {
  export interface Request {
    db: Connection
  }
}

let conn: Connection | undefined

/**
 * Attach a db connection to the request
 */
export async function dbConn (req: Request, res: Response, next: NextFunction) {
  try {
    if (!conn) conn = await createConnection(connectionOptions)
    req.db = conn
    next()
  } catch (error) {
    console.log(error)
    const payload = {
      statusCode: 500,
      error: 'Internal Server Error',
      message: 'Unable to connect to database'
    }
    return res.status(payload.statusCode).send(payload).end()
  }
}
