import { Repository, EntityRepository } from 'typeorm'
import { Team } from '../models'

@EntityRepository(Team)
export class TeamRepository extends Repository<Team> {
  /**
   * List all teams that a user belongs to
   * @param userId
   */
  public getTeamsForUser (userId: string) {
    return this.createQueryBuilder('team')
      .setParameters({ userId })
      .select(['owner.id', 'team.id', 'team.name'])
      // Only return teams that a user is a member of
      .innerJoin('team.users', 'user', 'user.id = :userId')
      .leftJoin('team.owner', 'owner')
      .orderBy('team.name')
      .getMany()
  }

  /**
   * Return a specified team if a specified user exists as a member
   * @param userId
   */
  public async getTeamForUser (teamId: number, userId: string) {
    console.log('Finding team', teamId, 'for user', userId)
    const team = await this.createQueryBuilder('team')
      .setParameters({ teamId, userId })
      .select([
        'team.name',
        'team.id',
        'owner.id',
        'owner.email',
        'user.email',
        'user.id'
      ])
      // Only return a team that a user is a member of
      .innerJoin('team.users', 'user', 'user.id = :userId')
      .leftJoin('team.owner', 'owner')
      .leftJoinAndSelect('team.categories', 'categories')
      .leftJoinAndSelect('categories.announcements', 'announcements')
      .where('team.id = :teamId')
      .getOne()

    return team
  }
}
