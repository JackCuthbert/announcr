import { Repository, EntityRepository } from 'typeorm'
import { User } from '../models'

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  /**
   * Return a user by a supplied email ignoring case
   * @param email
   * @param select Array of properties to select from the User class
   */
  public getUserByEmail (email: string, select: (keyof User)[] = []) {
    let query = this.createQueryBuilder('user')
    if (select.length !== 0) {
      query = query.select(select.map(s => `user.${s}`))
    }
    return query
      .setParameter('email', email)
      .where('LOWER(user.email) = LOWER(:email)')
      .getOne()
  }
}
