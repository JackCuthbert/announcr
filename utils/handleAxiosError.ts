import { Response } from 'express'

/**
 * Handle an axios error in a consistent manner
 */
export function handleAxiosError (error: any, res: Response) {
  if (error.response) {
    console.error(error.response)
    res.status(error.response.status).send(error.response.data).end()
  } else if (error.request) {
    console.error(error.request)
    res.status(500).end()
  } else {
    console.error('Error', error.message)
    res.status(500).send(error.message)
  }

  console.error(error.config)
}
