// Augment the Request module to cast params to an array of strings. Modofun
// passes through strings in order
declare module 'express' {
  export interface Request {
    params: string[]
  }
}

export { auth } from './functions/auth'
export { team } from './functions/team'
export { user } from './functions/user'
