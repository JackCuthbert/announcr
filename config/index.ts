import 'reflect-metadata'
import { ConnectionOptions } from 'typeorm'
import { Announcement, Category, User, Team } from '../models'

const {
  NODE_ENV = 'dev',

  INSTANCE_CONNECTION_NAME = '',
  SQL_USER = 'root',
  SQL_PASSWORD = 'root',
  SQL_NAME = 'announcr',

  JWT_SECRET = 'super_secret'
} = process.env

export const env = NODE_ENV

/** Post used for the test suite server */
export const testPort = 3001

/** Secret used to sign JWT */
export const jwtSecret = JWT_SECRET

/** TypeORM connection options */
export const connectionOptions: ConnectionOptions = {
  type: 'postgres',
  host: env === 'production'
    ? `/cloudsql/${INSTANCE_CONNECTION_NAME}`
    : 'localhost',
  username: SQL_USER,
  password: SQL_PASSWORD,
  database: SQL_NAME,
  // TODO: When in production we should use migrations instead
  // synchronize: env !== 'production',
  synchronize: true,
  entities: [
    User,
    Team,
    Announcement,
    Category
  ]
  // TODO: Implement migration system
  // migrations: [
  //   'migrations/*.ts'
  // ],
  // cli: {
  //   migrationsDir: 'migrations'
  // }
}
