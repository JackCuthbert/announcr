import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  OneToMany,
  ManyToOne
} from 'typeorm'
import { Team } from './Team'
import { Announcement } from './Announcement'

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @ManyToOne(_type => Team, team => team.categories, { onDelete: 'CASCADE' })
  @JoinColumn()
  team: Team

  @OneToMany(_type => Announcement, announcement => announcement.category)
  announcements: Announcement[]

  @Column({ nullable: true })
  slackWebhook?: string

  // NOTE: can only be one per team
  @Column('bool', { default: false })
  isDefault: boolean
}
