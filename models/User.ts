import {
  Entity,
  JoinTable,
  ManyToMany,
  Column,
  PrimaryGeneratedColumn
} from 'typeorm'
import { Team } from './Team'

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @ManyToMany(_type => Team, team => team.users)
  @JoinTable()
  teams: Team[]

  @Column({ unique: true })
  email: string

  @Column()
  password: string

  @Column({ nullable: true })
  displayName?: string
}
