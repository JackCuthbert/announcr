import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToMany,
  ManyToOne,
  JoinColumn,
  OneToMany
} from 'typeorm'
import { User } from './User'
import { Category } from './Category'

@Entity()
export class Team {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @ManyToOne(_type => User)
  @JoinColumn()
  owner: User

  @ManyToMany(_type => User, user => user.teams)
  users: User[]

  @OneToMany(_type => Category, category => category.team)
  categories: Category[]
}
