import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  ManyToOne
} from 'typeorm'
import { Category } from './Category'

@Entity()
export class Announcement {
  @PrimaryGeneratedColumn()
  id: number

  @ManyToOne(_type => Category, category => category.announcements)
  @JoinColumn()
  category: Category

  @Column()
  title: string

  /**
   * Markdown
   */
  @Column('text')
  body: string

  @Column({ default: false })
  published?: boolean
}
