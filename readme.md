# Announcr

(working title)

## Setup

Requirements for setup are as follows:

### Node

Node 8 is required.

### Database

The following will run a local instance of postgres and adminer.

```bash
docker-compose up -d
```

### Google Cloud

Install the Google Cloud SDK...

```bash
# on mac
brew install google-cloud-sdk

# on arch
sudo aura -Aa google-cloud-sdk
```

...and initilise some configuration.

```bash
gcloud init
```

Install the Google Cloud Functions Emulator...

```bash
npm install -g @google-cloud/functions-emulator
```

...and start it.

```bash
functions-emulator start
```

## Running

Install dependencies

```bash
npm install
```

Deploy functions to the emulator with:

```bash
functions-emulator deploy <function_name> --source=.build --trigger-http
```

or

```bash
./scripts/deploy-function <function_name>
```
